## RMX2020-user 11 RP1A.200720.011 V12.0.2.0.RJCMIXM release-keys
- Manufacturer: realme
- Platform: 
- Codename: galahad
- Brand: Realme
- Flavor: RMX2020-user
galahad-user
RMX2020-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: V12.0.2.0.RJCMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: undefined
- Fingerprint: Redmi/lancelot_global/lancelot:11/RP1A.200720.011/V12.0.2.0.RJCMIXM:user/release-keys
Redmi/galahad_global/galahad:11/RP1A.200720.011/V12.0.2.0.RJCMIXM:user/release-keys
Redmi/lancelot_global/lancelot:11/RP1A.200720.011/V12.0.2.0.RJCMIXM:user/release-keys
- OTA version: 
- Branch: RMX2020-user-11-RP1A.200720.011-V12.0.2.0.RJCMIXM-release-keys
- Repo: realme_galahad_dump_19087


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
